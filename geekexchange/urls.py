"""geekexchange URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, reverse_lazy
from django.urls import include

from geekexchange import settings
from django.conf.urls.static import static

def redirect_root(request):
    return redirect(reverse_lazy('login'))

urlpatterns = [
    path('', redirect_root),
    path('admin/', admin.site.urls),
    path('listing/', include('listing.urls')),
    path('accounts/', include('accounts.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('api/marketplace/', include('marketplace.urls')),
    path('marketplace/', include('marketplace_browser.urls')),
    path('subscription/', include('payment.urls'))

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

