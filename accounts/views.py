from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render, redirect

# Create your views here.
# accounts/views.py
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views.generic import CreateView


class SignUp(SuccessMessageMixin, CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'
    success_message = "Welcome to GeekExchange! You may sign in using your username and password."

    def dispatch(self, *args, **kwargs):
        if self.request.user is not None and self.request.user.is_authenticated:
            return redirect(reverse_lazy('listing-user'))
        return super().dispatch(*args, **kwargs)
