from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView


class BrowserView(TemplateView):
    template_name = "marketplace_browser/browser.html"
