from django.apps import AppConfig


class MarketplaceBrowserConfig(AppConfig):
    name = 'marketplace_browser'
