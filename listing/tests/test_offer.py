from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone

# Create your tests here.
from listing.models import Listing
from listing.models import Offer

class OfferModelTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user("user01", "user01@asd.com")
        self.buyer = User.objects.create_user("buyer01", "buyer01@asd.com")
        self.buyer2 = User.objects.create_user("buyer02", "buyer02@asd.com")


    def test_can_create_offer_for_existing_listing(self):
        listing = Listing(title="Saint Petersburg", owner=self.user,
                          text="First edition", amount=99.9, created_date=timezone.now())
        listing.save()

        offer = Offer(owner=self.buyer, listing=listing, amount=80)
        offer.save()

        retrieved = Offer.objects.filter(id=offer.id).get()
        self.assertIsNotNone(retrieved)
        self.assertEquals(retrieved.id, offer.id)
        self.assertEquals(retrieved.listing, offer.listing)
        self.assertEquals(retrieved.owner, self.buyer)
        self.assertEquals(retrieved.amount, offer.amount)

        listing = Listing.objects.filter(id=listing.id).get()
        self.assertEquals(len(listing.offer_set.all()), 1)

    def test_listing_can_have_many_offers(self):
        listing = Listing(title="Saint Petersburg", owner=self.user,
                          text="First edition", amount=99.9, created_date=timezone.now())
        listing.save()

        offer = Offer(owner=self.buyer, listing=listing, amount=80)
        offer.save()

        offer = Offer(owner=self.buyer2, listing=listing, amount=75)
        offer.save()

        listing = Listing.objects.filter(id=listing.id).get()
        self.assertEquals(len(listing.offer_set.all()), 2)