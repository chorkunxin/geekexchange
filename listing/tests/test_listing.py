from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone

# Create your tests here.
from listing.models import Listing


class ListingModelTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user("user01", "user01@asd.com")

    def test_can_save_model(self):
        listing = Listing(title="Saint Petersburg", owner=self.user,
                          text="First edition", amount=99.9, created_date=timezone.now())
        listing.save()

        retrieved = Listing.objects.filter(id=listing.id).get()
        self.assertIsNotNone(retrieved)
        self.assertEqual(retrieved.id, listing.id)
        self.assertEqual(retrieved.title, listing.title)
        self.assertEqual(retrieved.owner.id, listing.owner.id)
        self.assertEqual(retrieved.amount, listing.amount)
        self.assertEqual(retrieved.created_date, listing.created_date)

    def test_can_publish(self):
        listing = Listing(title="Saint Petersburg", owner=self.user,
                          text="First edition", amount=99.9, created_date=timezone.now())
        listing.save()
        listing.publish()
        retrieved = Listing.objects.filter(id=listing.id).get()
        self.assertEqual(retrieved.published_date.strftime("%Y-%m-%d %H:%M"), timezone.now().strftime("%Y-%m-%d %H:%M"))
