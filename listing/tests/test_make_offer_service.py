from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone

from listing.models import Listing
from listing.models import Offer
from listing.services.MakeOfferService import MakeOfferService


class MakeServiceTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user("user01", "user01@asd.com")
        self.buyer = User.objects.create_user("buyer01", "buyer01@asd.com")
        self.makeOfferService = MakeOfferService()

    def test_can_make_offer(self):
        listing = Listing(title="Saint Petersburg", owner=self.user,
                          text="First edition", amount=99.9, created_date=timezone.now())
        listing.save()

        created_offer = self.makeOfferService.make_offer(listing, self.buyer, 112)
        self.assertIsNotNone(created_offer)

        offer = Offer.objects.filter(listing=listing).get()
        self.assertEquals(offer.owner, self.buyer)
        self.assertEquals(offer.amount, 112)
