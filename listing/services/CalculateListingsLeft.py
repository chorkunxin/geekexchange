from listing.models import Listing
from payment.models import Payment


class CalculateListingsLeft:
    def calculate(self, user):
        listings = len(Listing.objects.filter(owner=user))
        payment_made = len(Payment.objects.filter(owner=user))
        return payment_made * 5 - listings

