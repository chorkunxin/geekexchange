from django.utils import timezone
from listing.models import Listing
from listing.models import Offer


class MakeOfferService:

    def make_offer(self, listing, offering_user, amount):
        if self.have_user_offered_before(listing, offering_user):
            offer = listing.offer_set.create(owner=offering_user, amount=amount)
            return offer
        return None

    def have_user_offered_before(self, listing, offering_user):
        found = Offer.objects.filter(listing=listing).filter(owner=offering_user)
        return found is not None
