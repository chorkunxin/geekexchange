from listing.models import Listing


class GetListing:
    def forUser(self, user):
        return Listing.objects.filter(owner=user)