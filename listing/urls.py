from django.urls import include
from django.urls import path
from . import views

urlpatterns = [
    path('', views.ListingPageView.as_view(), name='listing'),
    path('user', views.UserListingPageView.as_view(), name='listing-user'),
    path('create', views.AddListingPageView.as_view(), name='listing-add'),
    path('<listing_id>/update', views.EditListingPageView.as_view(), name='listing-update'),
    path('<listing_id>/delete', views.DeleteListingPageView.as_view(), name='listing-delete')
]