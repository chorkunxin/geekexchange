from listing.services.CalculateListingsLeft import CalculateListingsLeft


def credits_processor(request):
    if request.user.is_authenticated:
        c = CalculateListingsLeft()
        credits_left = c.calculate(request.user)
        return {
            'credits': credits_left
        }
    else:
        return {
            'credits': 0
        }
