from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, UpdateView, DeleteView, CreateView
from django.views.generic.base import TemplateView
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin

from listing.services.CalculateListingsLeft import CalculateListingsLeft
from listing.services.GetListing import GetListing
from .forms import ListingForm
from django.utils import timezone

from listing.models import Listing


class ListingPageView(LoginRequiredMixin, TemplateView):
    template_name = "home.html"


class UserListingPageView(LoginRequiredMixin, ListView):
    template_name = "listing/user_listing.html"
    model = Listing
    paginate_by = 20

    def get_queryset(self):
        get_listing = GetListing()
        return get_listing.forUser(self.request.user)


class AddListingPageView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    form_class = ListingForm
    template_name = 'listing/add_listing_page.html'
    success_message = 'Listing %(title) has been edited'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        c = CalculateListingsLeft()
        context['credits'] = c.calculate(self.request.user)
        return context

    def form_valid(self, form):
        listing = form.save(commit=False)
        listing.owner = self.request.user
        listing.published_date = timezone.now()
        listing.save()
        messages.success(self.request, message="New listing successfully added!")
        return redirect(reverse('listing-user'))


class EditListingPageView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Listing
    template_name = 'listing/listing_update_form.html'
    pk_url_kwarg = 'listing_id'
    success_message = 'Listing %(title) has been edited'
    form_class = ListingForm

    def get_success_url(self):
        return reverse('listing-user')

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
            title=self.object.title
        )


class DeleteListingPageView(LoginRequiredMixin, DeleteView):
    model = Listing
    success_url = reverse_lazy('listing-user')
    pk_url_kwarg = 'listing_id'
    context_object_name = 'listing'

