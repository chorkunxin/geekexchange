from django.db import models
from django.conf import settings
from django.utils import timezone
import uuid
import os

def rename_and_upload_file(instance, filename):
    path = "upload/listings/" + str(instance.owner.id)
    name, ext = os.path.splitext(filename)
    format = uuid.uuid4().hex + ext
    return os.path.join(path, format)


class Listing(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    text = models.TextField()
    amount = models.FloatField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    accepted_date = models.DateTimeField(blank=True, null=True)
    image = models.ImageField(upload_to=rename_and_upload_file, default='')

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class Offer(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE)
    amount = models.FloatField()
    accepted = models.BooleanField(default=False)
    created_date = models.DateTimeField(default=timezone.now)
    accepted_date = models.DateTimeField(blank=True, null=True)

    def accept(self):
        self.accepted_date = timezone.now()
        self.accepted = True
        self.save()

    def __str__(self):
        return "Offer #" + str(self.id)
