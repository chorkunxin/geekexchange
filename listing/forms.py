from django import forms
from django.core.exceptions import ValidationError

from .models import Listing


class ListingForm(forms.ModelForm):

    class Meta:
        model = Listing
        fields = ('title', 'text', 'amount', 'image')

    def clean(self):
        super(ListingForm, self).clean()

        title = self.cleaned_data.get('title')
        text = self.cleaned_data.get('text')
        amount = self.cleaned_data.get('amount')
        image = self.cleaned_data.get('image')

        if len(title) < 3:
            self._errors['title'] = "Please provide a title that is more than 3 characters"

        if len(text) == 0:
            self._errors['text'] = "Please provide a description"

        if float(amount) < 1:
            self._errors['amount'] = "Amount must be bigger than 0"

        if len(image) == 0 :
            self._errors['image'] ="Please provide a photo"

        return self.cleaned_data




