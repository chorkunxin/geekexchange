# GeekExchange Readme

GeekExchange is a marketplace that allows board game enthusiasts to post
their board games up for trade. Users can list their board games, and
submit offers for the ones that they are interested in.

##Installation
Requirements:
* Python 3.6
* pip

Installation steps:
1. Create a directory to store the project and apps
2. Change to that directory
3. Pull from the geekexchange repo
4. Change to the geekexchange directory
5. Do "pip install -r requirements.txt"
6. Do "python manage.py migrate"
7. Do "python manage.py runserver 8080"
8. Access at "http://localhost:8080"

**Testing**
Run "python manage.py test" to start unit tests.

##Technology Used
* Django 2
* SQLite
* VueJS

###External Apps Used
* Stripes
* REST Framework

##Features Implemented

* User sign up
* User login
* Routes that require authentication to access
* Stripe payment to purchase credits
* Credits system (1 posting requires 1 credit)
* Post listing
* Update listing
* Delete listing
* Update/post listing validation
* Browse other listings
* VueJS based listing browser

##Django features used
* Class based views
* Context processors
* Applications
* Django ORM
* Django templating




