from django.urls import path
from . import views

urlpatterns = [
    path('', views.SubscriptionPageView.as_view(), name='subscriptions'),
    path('charge', views.charge, name='charge')
]