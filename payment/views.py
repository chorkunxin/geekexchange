import stripe
from django.shortcuts import render

# Create your views here.
from django.utils import timezone
from django.views.generic import TemplateView

from geekexchange import settings
from payment.models import Payment

stripe.api_key = settings.STRIPE_SECRET_KEY


class SubscriptionPageView(TemplateView):
    template_name = "payment/subscriptions.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['key'] = settings.STRIPE_PUBLISHABLE_KEY
        return context


def charge(request):
    if request.method == 'POST':
        charge = stripe.Charge.create(
            amount=500,
            currency='sgd',
            description="Purchase for 5 credits",
            source=request.POST['stripeToken']
        )

        p = Payment(owner=request.user,
                    stripe_payment_id=charge.id,
                    date=timezone.now(),
                    amount=charge.amount
                    )
        p.save()

        return render(request, 'payment/charge.html')
