from django.db import models
from django.conf import settings

# Create your models here.
from django.utils import timezone


class Payment(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    stripe_payment_id = models.CharField(max_length=255)
    date = models.DateTimeField(default=timezone.now)
    amount = models.IntegerField()

    def __str__(self):
        return self.stripe_payment_id
