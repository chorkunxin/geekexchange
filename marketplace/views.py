# Create your views here.
from django.contrib.auth.models import User, Group
from rest_framework import viewsets

from .services.GetAvailableListings import GetAvailableListings
from .serializers import ListingSerializer, UserSerializer, GroupSerializer

# class AvailableListingView(ListView):
#     template_name = "marketplace/listing.html"
#     model = Listing
#     paginate_by = 20
#
#     def get_queryset(self):
#         get_available_listing = GetAvailableListings()
#         return get_available_listing.get()[:self.paginate_by]


class ListingViewSet(viewsets.ModelViewSet):
    get_available_listing = GetAvailableListings()
    queryset = get_available_listing.get()
    serializer_class = ListingSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer