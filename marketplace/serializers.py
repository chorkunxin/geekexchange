from django.contrib.auth.models import User, Group

from listing.models import Listing
from rest_framework import serializers


class ListingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Listing
        fields = ('id', 'url', 'owner', 'title', 'text', 'published_date', 'image', 'accepted_date')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')