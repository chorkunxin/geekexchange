from listing.models import Listing


class GetAvailableListings:

    def get(self):
        return Listing.objects.filter(accepted_date=None).order_by('-published_date')